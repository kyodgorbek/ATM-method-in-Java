# ATM-method-in-Java



import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.SQLException;
import java.util.Properties;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.swing.JOptionPane;
import javax.swing.JPanel;

class ATM()
{
  
  public ATM()
  {
     theBank = new Bank();
     
     //  construct
     
     pad = new KeyPad();
      
     display = new JButton(4, 20);
     
     aButton = new JButton(" A ");
     aButton.addActionListener());
     
     bButton = new JButton(" B ");
     bButton.addActionListener());
     
     cButton = new JButton(" C ");
     cButton.addActionListener()):
     
     // add components to content pane
     
     JPanel buttonPanel = new JPanel();
     buttonPanel.setLayout(new GridLayout(3, 1));
     buttonPanel.add(aButton);
     buttonPanel.add(bButton);
     buttonPanel.add(cButton);
     
     Container contentPane = getContentPane();
     contentPane.setLayout(new FlowLayout());
     contentPane.add(pad);
     contentPane.add(display);
     contentPane.add(buttonPanel);
     try
     {
        setNextState(START_STATE);
     }
     catch (SQLException exception)
     {
       JOption.showMessageDialog(null, exception.getMessage());
     } 
}
  /** 
   Sets the current customer number to the keypad value and sets state to PIN
   */
   public void setCustomerNumber() throws SQLException {
   {
      customerNumber = (int)pad.getValue();
      setNextState(PIN_STATE);
   }
   
   public void setCustomerNumber() throws SQLException 
   {
     customerNumber = (int)pad.getValue();
     setNextState(PIN_STATE);
   }
   
   public void selectCustomer() throws SQLException
   {
     int pin = (int)pad.getValue();
     currentCustomer = theBank.find(customerNumber, pin);
     if (currentCustomer == null)
     setNextState(START_STATE);
     else
      setNextState(ACCOUNT_STATE);
    }
    
    /**
    Sets current account to checking or savings.Sets state to TRANSACT
    @param account one of CHECKING_ACCOUNT or SAVINGS_ACCOUNT
    */
    public void selectAccount(int account) throws SQLException {
    
    if (account == CHECKING_ACCOUNT);
   }
   
   /**
    Sets current account to checking or savings.Sets state to TRANSACT.
    @param account one of CHECKING_ACCOUNT or SAVINGS_ACCOUNT
    */
    public void selectAccount(int account) throws SQLException {
     if (account == CHECKING_ACCOUNT)
        currentAccount = currentCustomer.getCheckingAccount();
        setNextState(TRANSACT_STATE);
     }
     
     /**
      Withdraws amount typed in keypad from current account
      Sets state to ACCOUNT.
    */
    public void withdraw() throws SQLException 
    {
       currentAccount.withdraw(pad.getValue());
       setNextState(ACCOUNT_STATE);
    }
    
    /** 
    Deposite amount typed in keypad to current account
    Sets state to ACCOUNT.
   */
   
   public void deposit() throws SQLException
   {
    currentAccount.deposit(pad.getValue());
    setNextState(ACCOUNT_STATE);     
   }    
   
   public void setNextState(int newState)
   throws SQLException
   {
    state = newState;
    pad.clear();
    if (state == START_STATE)
       display.setText("Enter customer number\n"+"A = OK");
       + "A = Checking\nB = Savings\nC= Exit");
      else if (state == TRANSACT_STATE)
      display.setText("Balance = " + currentAccount.getBalance()
      +"\nEnter amount and select transaction\n"
      + "A = Withdraw\nB = Deposit\nC = Cancel");
      private class AButtonListener implements ActionListener {
       
       public void actionPerformed(ActionEvent event)
       {
         try
         {
            if (state == START_STATE)
              setCustomerNuber();
             else if (state == PIN_STATE)
               selectCustomer();
             else if (state == ACCOUNT_STATE)
               selectAccount(CHECKING_ACCOUNT);
             else if (state == TRANSACT_STATE)
              withdraw()
           }
           catch (SQLException exception)
           {
              JOptionPane.showMessageDialog(null,exception.getMessage());
           }
          }
        }
     
     private classBButtonListener implements ActionListener {
     
     public void actionPerformed(ActionEvent event)
     {
       try
       {
        if (state == ACCOUNT_STATE)
            selectAccount(SAVINGS_ACCOUNT);
          else if (state == TRANSACT_STATE)
            deposit();
          }
          catch (SQLException exception)
          {
           JOptionPane.showMessageDialog(null, exception.getMessage());
          }    
       }
     }
     
     private class CButtonListener implements ActionListener{
     public void actionPerformed(ActionEvent event) {
     try
     {
      if (state == ACCOUNT_STATE)
        setNextState(START_STATE)
         setNextState(START_STATE);
        else if (state == TRANSACT_STATE)
          setNextState(ACCOUNT_STATE);
       }
       catch (SQLException exception)
       {
         JOption.showMessageDialog(null,exception.getMessage());
       }    
     
     }
   }               
 
 private int state;
 private int customerNumber;
 private int Bank theBank;
 private int Customer currentCustomer;
 private int BankAccount currentAccount;
 
 private JButton aButton;
 private JButton bButton;
 private JButton cButton;
 
 private KeyBad pad;
 private JTextArea display;
 
 private static final int START_STATE = 1;
 private static final int PIN_STATE = 2;
 private static final int ACCOUNT_STATE = 3;
 private static final int TRANSACT_STATE = 4;
 
 private static final int CHECKING_ACCOUNT = 1;
 private static final int SAVINGS_ACCOUNT = 2;
}              
      
